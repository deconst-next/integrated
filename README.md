# Deconst, Integrated

Deconst Integrated is a
[`docker-compose`](https://docs.docker.com/compose/) configuration that instantiates
a single collection of Deconst services, with the debug settings cranked up to full.
It's useful for:

* Integration testing of Deconst components, before you commit to master and :shipit:
* Previewing local content or control repository changes

## Prerequisites

### OSX

* Install a recent version of
   [Docker](https://docs.docker.com/installation/#installation)

### docker-compose
The new docker-compose-fullstack.yml configuration already has everything in place to successfully boot up deconst microservices automatically and sets up each one accordingly.

Clone this repo to your local env and run
```
docker-compose -f docker-compose-fullstack.yml up -d
```
The -d simply detach and runs the result independently of your tty. If you need to debug any of the services in the docker-compose file it will be simpler to use the command without the `-d` opcion as you will be able to see the logs output of each service directly in your cli.

`Note.` For now you will be using the memory storage type until we issue a fix to connect to mongodb, as mongodb connection forms has introduced a breaking change that halts the use of `hybrid` storage kind.

Once you have the services up locally you can build and submit content as you normally would with submitter.

#### Build content from a repository using new preparer docker image from gitlab

To build the docs-developer-blog repo that uses jekyll preparer follow this instructions. The same pattern applies for other repos.

1. Clone the repo.
2. Configure the `CONTENT_ID_BASE` env to that of the repository. Its imporant to add `/test` to the repository id base regular value because the local environment control repo routing is configured to use it.

I.E
```
export CONTENT_ID_BASE=https://github.com/rackerlabs/docs-developer-blog/test
```

#### Build content using preparer-jekyll
```
docker run -e CONTENT_ID_BASE=$CONTENT_ID_BASE -e ENVELOPE_DIR=_build/envelopes -e ASSET_DIR=_build/assets -e CONTENT_ROOT=/content -v /path/to/cloned_dir:/content registry.gitlab.com/deconst-next/preparer-jekyll:latest
```

#### Submit ready content
```
docker run -e CONTENT_SERVICE_URL=http://localhost:9000 -e CONTENT_SERVICE_APIKEY=123456 -e CONTENT_ID_BASE=$CONTENT_ID_BASE -e ENVELOPE_DIR=/content/_build/envelopes -e ASSET_DIR=/content/_build/assets  -v /path/to/cloned_dir:/content registry.gitlab.com/deconst-next/submitter:latest
```

#### Nexus control repository for displaying local content.
The docker-compose.yml file is already configure to use the `local-routes` branch of nexus-control remote repository, this adds a few routing files that points to `developer.local.deconst.org` and `support.local.deconst.org` local domains.

If you want to mount your own control repository simply comment `CONTROL_REPO_URL`, enable the volumes lines on each presenter, change the branch to the one you want, i.e, `master` and add value to the environment variable `CONTROL_REPO_HOST_PATH` with the local directory path of your cloned nexus-control repository.

#### Local tests domains
You must add the following entries to your `/etc/hosts` file to simulate the proper domains for `developer` and `support` sits locally. This is important to be able to display content when accesing the presenter for each domain. Both domains are already preconfigured to each presenter that spins up in the docker compose configuration.

```
127.0.0.1 support.local.deconst.org
127.0.0.1 developer.local.deconst.org
```


### Windows or Linux

 1. Install [docker-compose](https://docs.docker.com/compose/install/)
 1. Create a `docker-machine dev` instance:

    ```bash
    docker-machine create --driver virtualbox dev
    ```

 1. Run `eval $(docker-machine env default)` in each shell that you'll
    use to interact with Docker.

### GitLab

The `deconst-next` project requires two-factor authentication to push containers to the GitLab registry. For information about the GitLab container registry, see
<https://gitlab.com/deconst-next/integrated/container_registry>.

Container registry access is not necessary if you plan to use Deconst Integrate for local testing only.

## Edit the env file

Clone this Deconst Integrated repository and change to the directory where you cloned the repo.
Customize your credentials and other settings in the `env` file. Setting these variables requires knowing your account info and where you intend to clone the control repo.

   1. Copy the example file to `env`:

      ```bash
      cp env.example env
      ```

   1. Edit the `env` file in a text editor and change it as
      appropriate for your environment.

   1. Set the domain name of the published documentation site:

      ```bash
      # Set this to the domain name of the site you're interested in.
      export PRESENTED_URL_DOMAIN=deconst.org
      ```

   1. Set the location of your control repository. For
      example, for `deconst.org`:

      ```bash
      # Set this to a path to a control repository on your local
      # machine to preview local changes to a control repository.
      unset CONTROL_REPO_HOST_PATH
      export CONTROL_REPO_HOST_PATH="/Users/writer1/docs-control"
      ```

## Start integrated services

Type the following command to launch the Deconst services:

    `script/up`

The `script/up` command accepts any parameters that `docker-compose
up` does. Notably, you can use `script/up -d` to launch services in
the background.

### Option: Run the Docker containers manually

As an alternative to the `up` script, you can manually run each Docker
container, as follows:

```bash
# generate an admin API key for the content service
APIKEY=$(hexdump -v -e '1/1 "%.2x"' -n 128 /dev/random)
echo "Content Service Admin API Key:" $APIKEY

# start content service dependencies
docker run -d --name elasticsearch elasticsearch:6.2.1
docker run -d --name mongo mongo:3.6

# build and deploy the content service
cd ${CODE_ROOT}/content-service
docker build --tag content-service:dev .
docker run -d -p 9000:8080 \
  -e NODE_ENV=development \
  -e STORAGE=memory \
  -e MONGODB_URL=mongodb://mongo:27017/content \
  -e ELASTICSEARCH_HOST=http://elasticsearch:9200/ \
  -e ADMIN_APIKEY=${APIKEY} \
  --link mongo:mongo \
  --link elasticsearch:elasticsearch \
  --name content \
  content-service:dev script/inside/dev

# build and deploy the presenter service
cd ${CODE_ROOT}/presenter
docker build --tag presenter:dev .
docker run -d -p 80:8080 \
  -e NODE_ENV=development \
  -e CONTROL_REPO_PATH=/var/control-repo \
  -e CONTROL_REPO_URL=... \
  -e CONTROL_REPO_BRANCH=... \
  -e CONTENT_SERVICE_URL=http://content:8080 \
  -e PRESENTED_URL_PROTO=http \
  -e PRESENTED_URL_DOMAIN=deconst.org \
  --link content \
  --name presenter \
  presenter:dev script/dev
```

### Submitting content

Now the site is running, but you don't have any content submitted,
yet. To add some, open a new terminal window and run the appropriate `script/add-*` script with the
path to your clone of a local content repository.

```bash
# (do this first to render styling)
# add the control repository:
script/add-assets ~/writing/drc/docs-control

# add a Sphinx site:
script/add-sphinx ~/writing/drc/docs-quickstart

# add a Jekyll site:
script/add-jekyll ~/writing/drc/docs-developer-blog
```

#### Updating content or mappings

If you make changes to the control repo — including content mapping,
template routing, redirects, or asset/template content — you must
restart the _presenter_ so it can pick up these changes. Run
`script/refresh` to restart the presenter.

## Cleaning up

To shut down all containers, type:

```bash
docker stop $(docker ps -a -q)
```
